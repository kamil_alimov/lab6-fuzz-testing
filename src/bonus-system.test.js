import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");


describe('Bonus system tests', () => {
    let app;
    console.log("Tests started");
    
    test('Standard Equality Test 1',  (done) => {
      let a = "Standard";
      let b = 10000;
      expect(calculateBonuses(a, b)).toEqual(0.05*1.5);
      done();
    });
    test('Standard Equality Test 2',  (done) => {
      let a = "Standard";
      let b = 50000;
      expect(calculateBonuses(a, b)).toEqual(0.05*2);
      done();
    });
    
    test('Standard Equality Test 3',  (done) => {
      let a = "Standard";
      let b = 100000;
      expect(calculateBonuses(a, b)).toEqual(0.05*2.5);
      done();
    });
    test('Nonsense Test',  (done) => {
      let a = "Nonsense";
      let b = -1000;
      expect(calculateBonuses(a, b)).toEqual(0);
      done();
    });
    
    test('Standard Test 1',  (done) => {
      let a = "Standard";
      let b = 5000;
      expect(calculateBonuses(a, b)).toEqual(0.05 * 1);
      done();
    });
    
    test('Standard Test 2',  (done) => {
      let a = "Standard";
      let b = 45000;
      expect(calculateBonuses(a, b)).toEqual(0.05*1.5);
      done();
    });
    
    test('Standard Test 3',  (done) => {
      let a = "Standard";
      let b = 95000;
      expect(calculateBonuses(a, b)).toEqual(0.05*2);
      done();
    });
    
    test('Standard Test 4',  (done) => {
      let a = "Standard";
      let b = 105000;
      expect(calculateBonuses(a, b)).toEqual(0.05*2.5);
      done();
    });
    
        test('Premium Test 1',  (done) => {
      let a = "Premium";
      let b = 5000;
      expect(calculateBonuses(a, b)).toEqual(0.1 * 1);
      done();
    });
    
    test('Premium Test 2',  (done) => {
      let a = "Premium";
      let b = 45000;
      expect(calculateBonuses(a, b)).toEqual(0.1 * 1.5);
      done();
    });
    
    test('Premium Test 3',  (done) => {
      let a = "Premium";
      let b = 95000;
      expect(calculateBonuses(a, b)).toEqual(0.1 *2);
      done();
    });
    
    test('Premium Test 4',  (done) => {
      let a = "Premium";
      let b = 105000;
      expect(calculateBonuses(a, b)).toEqual(0.1 * 2.5);
      done();
    });
    
    test('Diamond Test 1',  (done) => {
      let a = "Diamond";
      let b = 5000;
      expect(calculateBonuses(a, b)).toEqual(0.2 * 1);
      done();
    });
    
    test('Diamond Test 2',  (done) => {
      let a = "Diamond";
      let b = 45000;
      expect(calculateBonuses(a, b)).toEqual(0.2 * 1.5);
      done();
    });
    
    test('Diamond Test 3',  (done) => {
      let a = "Diamond";
      let b = 95000;
      expect(calculateBonuses(a, b)).toEqual(0.2 *2);
      done();
    });
    
    test('Diamond Test 4',  (done) => {
      let a = "Diamond";
      let b = 105000;
      expect(calculateBonuses(a, b)).toEqual(0.2 * 2.5);
      done();
    });
    
    console.log('Tests Finished');

});

